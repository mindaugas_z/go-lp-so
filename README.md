# go-lp-so

moved to [[https://bitbucket.org/mindaugas_z/go-silent-snake/src/master/libs/]]

golang set value once 


```go
func main() {
	so := lpso.New()

	if so.Set("content") {
		println("do something")
	}
	if so.Set("another content") {
		println("another job")
	} else {
        println("value:", so.Get().(string))
    }

	so.Reset()

	if so.Set("another content") {
		println("another job:", so.Get().(string))
	}
}
```
